# Nuxt 3 Minimal Starter + GitLab Pages(CI/CD)

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.

## GitLab Pages Deployment

This project is auto deploied to follow GitLab Pages.  
[https://gwktomoyoshi.gitlab.io/nuxt-gitlab-pages/](https://gwktomoyoshi.gitlab.io/nuxt-gitlab-pages/)

To deployment use follow .gitlab-ci.yml.
```yml
image: node

before_script:
  - npm install

pages:
  script:
    - npm run generate
    - cp -r .output/public .
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```
